/* eslint-disable no-template-curly-in-string */
module.exports = {
  presets: ['babel-preset-gatsby'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          config: './config',
          assets: './src/assets',
          components: './src/components',
          contents: './src/contents',
          views: './src/views',
          hooks: './src/hooks',
          templates: './src/templates',
          utils: './src/utils',
        },
      },
    ],
  ],
  env: {
    production: {
      plugins: ['transform-react-remove-prop-types'],
    },
  },
};
