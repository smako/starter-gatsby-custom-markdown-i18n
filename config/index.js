module.exports = {
  title: '',
  shortName: '',
  keywords: [''],
  description: '',
  author: '',
  trackingId: '', // ID for "gatsby-plugin-google-analytics"
  defaultLang: 'en',
  langTextMap: {
    en: 'English',
  },
};
