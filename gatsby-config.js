const { title, shortName, keywords, description, author, defaultLang, trackingId } = require('./config');

module.exports = {
  siteMetadata: {
    title,
    keywords,
    description,
    author,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'markdown',
        path: `${__dirname}/src/contents/`,
      },
    },
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        data: `@import "core.scss";`,
        includePaths: [`${__dirname}/src/assets/styles`],
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: title,
        short_name: shortName,
        description,
        lang: defaultLang,
        icon: 'src/assets/images/logo.png',
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        localize: [],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyDefault: defaultLang,
        useLangKeyLayout: false,
        pagesPaths: [`/src/contents/`],
      },
    },
  ],
};
