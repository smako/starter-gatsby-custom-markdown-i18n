const path = require('path');
const getBaseUrl = require('./src/utils/getBaseUrl');
const { defaultLang, langTextMap = {} } = require('./config');

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions;

  // Add fileName & directoryName of markdown file to the node
  if (node.internal.type === 'MarkdownRemark') {
    const fileName = path.basename(node.fileAbsolutePath, '.md');
    createNodeField({
      node,
      name: 'fileName',
      value: fileName,
    });

    createNodeField({
      node,
      name: 'directoryName',
      value: path.basename(path.dirname(node.fileAbsolutePath)),
    });
  }
};

/**
 * Generate i18n pages
 */
exports.createPages = async ({ graphql, actions: { createPage } }) => {
  const HomeIndex = path.resolve('./src/pages/Home/index.jsx');

  const { data } = await graphql(`
    {
      allMarkdownRemark {
        distinct(field: fields___langKey)
      }
    }
  `);

  data.allMarkdownRemark.distinct.forEach((langKey) => {
    /**
     * Generate Home Page
     */
    const baseUrl = getBaseUrl(defaultLang, langKey);
    createPage({
      path: `${baseUrl}`,
      component: HomeIndex,
      context: {
        langKey,
        defaultLang,
        langTextMap,
      },
    });
  });
};
