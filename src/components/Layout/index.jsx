/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

import SEO from 'components/SEO';
import { defaultLang } from 'config/';

const Layout = ({ title, description, lang, meta, keywords, children }) => {
  return (
    <>
      <SEO lang={lang} title={title} keywords={keywords} meta={meta} description={description} />
      <main>{children}</main>
    </>
  );
};

Layout.propTypes = {
  lang: PropTypes.string,
  description: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

Layout.defaultProps = {
  lang: defaultLang,
  meta: [],
  keywords: [],
  description: '',
};

export default Layout;
