import React from 'react';
import 'assets/styles/main.scss';
import './style.scss';

import PropTypes from 'prop-types';
import { graphql } from 'gatsby';

import breakDownAllNodes from 'utils/breakDownAllNodes';

import Layout from 'components/Layout';

import { defaultLang } from 'config/';

export const query = graphql`
  query($langKey: String) {
    allMarkdownRemark(filter: { fields: { langKey: { eq: $langKey } } }, sort: { order: ASC, fields: fields___fileName }) {
      nodes {
        frontmatter {
          anchor
          route
        }
        fields {
          fileName
          directoryName
        }
      }
    }
  }
`;

const HomePage = ({ data, pathContext }) => {
  const { nodes } = data.allMarkdownRemark;
  const { langKey } = pathContext;

  // const { } = breakDownAllNodes(nodes);

  return (
    <Layout title='Home' lang={langKey}>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', width: '100vw', height: '100vh' }}>
        <h1>Home Page</h1>
        <h2 style={{ fontWeight: 'lighter' }}>
          Ready to start
          <span role='img' aria-label='Rocket' style={{ marginLeft: '0.5rem' }}>
            🚀
          </span>
        </h2>
      </div>
    </Layout>
  );
};

HomePage.propTypes = {
  data: PropTypes.object.isRequired,
  pathContext: PropTypes.object,
};

HomePage.defaultProps = {
  pathContext: {
    langKey: defaultLang,
    defaultLang,
    langTextMap: {},
  },
};

export default HomePage;
