import { curry, pathSatisfies, test, identity, path } from 'ramda';

const propFilter = curry((pathList, regex) => pathSatisfies(test(regex), pathList));

/**
 * break down all data retrieved in index.js
 */
export default function breakDownAllNodes(nodes) {
  const filterByFileName = propFilter(['fields', 'fileName']);
  const filterByDirectoryName = propFilter(['fields', 'directoryName']);

  return {};
}
